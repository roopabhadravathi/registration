package com.philips.registration

import androidx.room.Room
import androidx.test.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.philips.database.AppDatabase
import com.philips.model.User
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class UserDaoTest {

    private lateinit var appDatabase: AppDatabase

    @Before
    fun initDb() {
        appDatabase = Room.inMemoryDatabaseBuilder(
            InstrumentationRegistry.getContext(),
            AppDatabase::class.java
        ).build()
    }

    @After
    fun closeDb() {
        appDatabase.close()
    }

    @Test
    fun testInsert() {
        val user = User("testing", "12345", "developer")
        val value = appDatabase.userDao().insert(user)
        assert(value >= 0)
    }

    @Test
    fun testGetUsers() {
        val user = User("testing", "12345", "developer")
        appDatabase.userDao().insert(user)
        val value = appDatabase.userDao().getUsers()
        assert(value.isNotEmpty())
    }

    @Test
    fun testFindByUser() {
        val user = User("testing", "12345", "developer")
        appDatabase.userDao().insert(user)
        val value = appDatabase.userDao().findByUser("testing", "12345")
        Assert.assertNotNull(value)
        Assert.assertEquals("testing", value?.email)
        Assert.assertEquals("12345", value?.password)
    }
}