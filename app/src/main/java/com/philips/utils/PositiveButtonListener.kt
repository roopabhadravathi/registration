package com.philips.utils

interface PositiveButtonListener {
    fun onClick()
}