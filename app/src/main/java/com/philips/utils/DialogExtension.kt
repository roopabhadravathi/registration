package com.philips.utils

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

fun AppCompatActivity.showAlert(message: String, isListenerRequired: Boolean = false) {
    val builder = AlertDialog.Builder(this)
        .setMessage(message)
        .setPositiveButton("Ok") { dialog, _ ->
            dialog.dismiss()
            if (isListenerRequired) {
                val listener: PositiveButtonListener = this as PositiveButtonListener
                listener.onClick()
            }
        }

    val dialog: AlertDialog = builder.create()
    dialog.show()
}