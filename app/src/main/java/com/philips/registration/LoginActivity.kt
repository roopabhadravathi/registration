package com.philips.registration

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import com.philips.model.ProcessingState
import com.philips.model.State
import com.philips.model.User
import com.philips.utils.showAlert
import com.philips.viewmodel.RegistrationViewModel
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_registration.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var registrationViewModel: RegistrationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        registrationViewModel = RegistrationViewModel(application)
        login.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        val email = loginEmail.text.toString()
        val password = loginPassword.text.toString()
        val isValid = registrationViewModel.validateFields(email, password)
        if (isValid) {
            val user = User(email, password)
            registrationViewModel.checkLoginUser(user).observe(this, Observer { onLoginResponse(it) })
        } else {
            showAlert(getString(R.string.mandatory_message))
        }
    }

    private fun onLoginResponse(state: ProcessingState) {
        if (state.state == State.SUCCESS) {
            showAlert(getString(R.string.login_success))
        } else {
            showAlert(getString(R.string.invalid_credentials))
        }
    }
}