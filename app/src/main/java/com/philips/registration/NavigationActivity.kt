package com.philips.registration

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.philips.model.ProcessingState
import com.philips.model.State
import com.philips.viewmodel.RegistrationViewModel


class NavigationActivity : AppCompatActivity() {

    private lateinit var registrationViewModel: RegistrationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        registrationViewModel = RegistrationViewModel(application)
        registrationViewModel.fetchRegisteredUsers().observe(this, Observer { onFetchUsers(it) })
    }

    private fun onFetchUsers(state: ProcessingState) {
        val intent: Intent = if (state.state == State.SUCCESS) {
            Intent(this, LoginActivity::class.java)
        } else {
            Intent(this, RegistrationActivity::class.java)
        }
        startActivity(intent)
        finish()
    }
}