package com.philips.registration

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import com.philips.model.ProcessingState
import com.philips.model.State
import com.philips.model.User
import com.philips.utils.PositiveButtonListener
import com.philips.utils.showAlert
import com.philips.viewmodel.RegistrationViewModel
import kotlinx.android.synthetic.main.activity_registration.*

class RegistrationActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener, View.OnClickListener, PositiveButtonListener  {

    private lateinit var registrationViewModel: RegistrationViewModel
    private lateinit var selectedRole: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        init()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        // Nothing to do onNothingSelected
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        selectedRole = spinner.selectedItem.toString()
    }

    override fun onClick(v: View?) {
        val email = email.text.toString()
        val password = password.text.toString()
        val isValid = registrationViewModel.validateFields(email, password)
        if (isValid) {
            val user = User(email, password, selectedRole)
            registrationViewModel.registerNewUser(user).observe(this, Observer { onRegistration(it) })
        } else {
            showAlert("Please enter mandatory fields")
        }
    }

    override fun onClick() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun onRegistration(state: ProcessingState) {
        if (state.state == State.SUCCESS) {
            showAlert(getString(R.string.registered_success), true)
        } else {
            showAlert(getString(R.string.registration_failed))
        }
    }

    private fun init() {
        registrationViewModel = RegistrationViewModel(application)
        spinner.onItemSelectedListener = this
        val roles = resources.getStringArray(R.array.roles)
        selectedRole = roles.get(0)
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, roles)
        spinner.adapter = adapter
        register.setOnClickListener(this)
    }
}