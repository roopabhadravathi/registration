package com.philips.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.room.Room
import com.philips.database.AppDatabase
import com.philips.model.ProcessingState
import com.philips.model.User
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers

class RegistrationViewModel(application: Application) : AndroidViewModel(application) {

    private var database: AppDatabase =
        Room.databaseBuilder(getApplication(), AppDatabase::class.java, "registration")
            .build()

    fun validateFields(email: String, password: String): Boolean {
        return email.isNotEmpty() && password.isNotEmpty()
    }

    fun registerNewUser(user: User): MutableLiveData<ProcessingState> {
        val liveData: MutableLiveData<ProcessingState> = MutableLiveData()
        saveUser(user).observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .doOnError { liveData.postValue(ProcessingState.errorState) }
            .subscribe(liveData::postValue) { liveData.postValue(ProcessingState.errorState) }
        return liveData
    }

    fun fetchRegisteredUsers(): MutableLiveData<ProcessingState> {
        val liveData: MutableLiveData<ProcessingState> = MutableLiveData()
        fetchUsers().observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .doOnError { liveData.postValue(ProcessingState.errorState) }
            .subscribe(liveData::postValue) { liveData.postValue(ProcessingState.errorState) }
        return liveData
    }

    fun checkLoginUser(user: User): MutableLiveData<ProcessingState> {
        val liveData: MutableLiveData<ProcessingState> = MutableLiveData()
        checkUser(user).observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .doOnError { liveData.postValue(ProcessingState.errorState) }
            .subscribe(liveData::postValue) { liveData.postValue(ProcessingState.errorState) }
        return liveData
    }

    private fun saveUser(user: User): Single<ProcessingState> {
        return Single.create<ProcessingState> {
            val value = database.userDao().insert(user)
            if (value >= 0) {
                ProcessingState.successState.extras = value
                it.onSuccess(ProcessingState.successState)
            } else {
                it.onError(ProcessingState.errorState.error)
            }
        }
    }

    private fun fetchUsers(): Single<ProcessingState> {
        return Single.create<ProcessingState> {
            val value = database.userDao().getUsers()
            if (value.isNotEmpty()) {
                it.onSuccess(ProcessingState.successState)
            } else {
                it.onError(ProcessingState.errorState.error)
            }
        }
    }

    private fun checkUser(user: User): Single<ProcessingState> {
        return Single.create<ProcessingState> {
            val value = database.userDao().findByUser(user.email, user.password)
            if (value != null) {
                it.onSuccess(ProcessingState.successState)
            } else {
                it.onError(ProcessingState.errorState.error)
            }
        }
    }
}