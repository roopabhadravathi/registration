package com.philips.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.philips.model.User

@Dao
interface UserDao {
    @Insert
    fun insert(user: User): Long

    @Query("SELECT * FROM user")
    fun getUsers(): List<User>

    @Query("SELECT * FROM user WHERE email LIKE :email AND " +
            "password LIKE :password LIMIT 1")
    fun findByUser(email: String, password: String): User?
}