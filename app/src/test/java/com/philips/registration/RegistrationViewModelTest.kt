package com.philips.registration

import android.app.Application
import com.philips.viewmodel.RegistrationViewModel
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class RegistrationViewModelTest {

    private lateinit var application: Application

    lateinit var registrationViewModel: RegistrationViewModel

    @Before
    fun setUp() {
        application = Application()
        registrationViewModel = RegistrationViewModel(application)
    }

    @Test
    fun testValidateFieldsTrue() {
        val isValid = registrationViewModel.validateFields("Testing", "12345")
        Assert.assertTrue(isValid)
    }

    @Test
    fun testValidateFieldsFalse() {
        val isValid = registrationViewModel.validateFields("Testing", "")
        Assert.assertFalse(isValid)
    }

    @Test
    fun testEmptyFieldsFalse() {
        val isValid = registrationViewModel.validateFields("", "")
        Assert.assertFalse(isValid)
    }
}