package com.philips.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0010\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001\u00a2\u0006\u0002\u0010\u0007R\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0001X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013\u00a8\u0006\u0015"}, d2 = {"Lcom/philips/model/ProcessingState;", "", "state", "Lcom/philips/model/State;", "error", "", "extras", "(Lcom/philips/model/State;Ljava/lang/Throwable;Ljava/lang/Object;)V", "getError", "()Ljava/lang/Throwable;", "setError", "(Ljava/lang/Throwable;)V", "getExtras", "()Ljava/lang/Object;", "setExtras", "(Ljava/lang/Object;)V", "getState", "()Lcom/philips/model/State;", "setState", "(Lcom/philips/model/State;)V", "Companion", "app_debug"})
public final class ProcessingState {
    @org.jetbrains.annotations.NotNull()
    private com.philips.model.State state;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Throwable error;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Object extras;
    @org.jetbrains.annotations.NotNull()
    private static final com.philips.model.ProcessingState errorState = null;
    @org.jetbrains.annotations.NotNull()
    private static final com.philips.model.ProcessingState successState = null;
    public static final com.philips.model.ProcessingState.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.philips.model.State getState() {
        return null;
    }
    
    public final void setState(@org.jetbrains.annotations.NotNull()
    com.philips.model.State p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Throwable getError() {
        return null;
    }
    
    public final void setError(@org.jetbrains.annotations.Nullable()
    java.lang.Throwable p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getExtras() {
        return null;
    }
    
    public final void setExtras(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
    }
    
    public ProcessingState(@org.jetbrains.annotations.NotNull()
    com.philips.model.State state, @org.jetbrains.annotations.Nullable()
    java.lang.Throwable error, @org.jetbrains.annotations.Nullable()
    java.lang.Object extras) {
        super();
    }
    
    /**
     * This static properties used to return the response state after the API call.
     */
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006\u00a8\u0006\t"}, d2 = {"Lcom/philips/model/ProcessingState$Companion;", "", "()V", "errorState", "Lcom/philips/model/ProcessingState;", "getErrorState", "()Lcom/philips/model/ProcessingState;", "successState", "getSuccessState", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.philips.model.ProcessingState getErrorState() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.philips.model.ProcessingState getSuccessState() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}