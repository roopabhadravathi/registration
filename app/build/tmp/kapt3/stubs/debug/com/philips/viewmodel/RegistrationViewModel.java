package com.philips.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\n\u001a\u00020\u000bJ\u0016\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\r2\u0006\u0010\n\u001a\u00020\u000bH\u0002J\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\t0\bJ\u000e\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\t0\rH\u0002J\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\n\u001a\u00020\u000bJ\u0016\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\t0\r2\u0006\u0010\n\u001a\u00020\u000bH\u0002J\u0016\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0015R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"}, d2 = {"Lcom/philips/viewmodel/RegistrationViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "database", "Lcom/philips/database/AppDatabase;", "checkLoginUser", "Landroidx/lifecycle/MutableLiveData;", "Lcom/philips/model/ProcessingState;", "user", "Lcom/philips/model/User;", "checkUser", "Lio/reactivex/rxjava3/core/Single;", "fetchRegisteredUsers", "fetchUsers", "registerNewUser", "saveUser", "validateFields", "", "email", "", "password", "app_debug"})
public final class RegistrationViewModel extends androidx.lifecycle.AndroidViewModel {
    private com.philips.database.AppDatabase database;
    
    public final boolean validateFields(@org.jetbrains.annotations.NotNull()
    java.lang.String email, @org.jetbrains.annotations.NotNull()
    java.lang.String password) {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.philips.model.ProcessingState> registerNewUser(@org.jetbrains.annotations.NotNull()
    com.philips.model.User user) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.philips.model.ProcessingState> fetchRegisteredUsers() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.philips.model.ProcessingState> checkLoginUser(@org.jetbrains.annotations.NotNull()
    com.philips.model.User user) {
        return null;
    }
    
    private final io.reactivex.rxjava3.core.Single<com.philips.model.ProcessingState> saveUser(com.philips.model.User user) {
        return null;
    }
    
    private final io.reactivex.rxjava3.core.Single<com.philips.model.ProcessingState> fetchUsers() {
        return null;
    }
    
    private final io.reactivex.rxjava3.core.Single<com.philips.model.ProcessingState> checkUser(com.philips.model.User user) {
        return null;
    }
    
    public RegistrationViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super(null);
    }
}