/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.philips.registration.test;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.philips.registration.test";
  public static final String BUILD_TYPE = "debug";
}
